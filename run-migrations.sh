#!/bin/bash
set -e

for I in /tmp/migrations/*; do
   /usr/bin/migrate -database 'clickhouse://localhost:9000/database=sdc_analytics?x-multi-statement=true&x-migrations-table-engine=MergeTree' -path ./tmp/migrations up
done

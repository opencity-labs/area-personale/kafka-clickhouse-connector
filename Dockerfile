# Usa un'immagine Python ufficiale come base
FROM python:3.11-slim

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Copia il file requirements.txt (se esiste) e lo script nella directory di lavoro
COPY requirements.txt .
COPY /src/tenants-to-clickhouse.py /app/src/tenants-to-clickhouse.py

# Installa le dipendenze del Python
RUN pip install --no-cache-dir -r requirements.txt

# Imposta la directory di lavoro all'interno del container
WORKDIR /app/src

# Comando per eseguire lo script Python
CMD [ "/app/src/api-to-clickhouse.py" ]
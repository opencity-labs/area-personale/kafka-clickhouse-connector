CREATE TABLE sdc_analytics.documents (
    id UUID,
    tenant_id UUID,
    remote_id UUID,
    title String,
    type String,
    status String,
    transmission_type String,
    registration_date Nullable(DateTime),
    registration_document_number String,
    remote_collection_id String,
    folder_id String,
    folder_title String,
    created_at DateTime
) ENGINE = ReplacingMergeTree(created_at)
ORDER BY id;
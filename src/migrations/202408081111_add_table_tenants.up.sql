CREATE TABLE sdc_analytics.tenants (
    id UUID,
    name String
) ENGINE = ReplacingMergeTree(id)
ORDER BY id;
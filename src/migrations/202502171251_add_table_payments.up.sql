CREATE TABLE sdc_analytics.payments (
    id UUID,
    user_id UUID,
    tenant_id UUID,
    app_id String,
    service_id UUID,
    payment_type String,
    status String,
    amount Float64,
    currency String,
    notice_code String,
    iuv String,
    due_type String,
    pagopa_category String,
    reason String,
    receiver_name String,
    receiver_tax_id String,
    paid_at Nullable(DateTime),
    expire_at Nullable(DateTime),
    created_at DateTime,
    updated_at DateTime,
    event_created_at DateTime
) ENGINE = ReplacingMergeTree(event_created_at)
ORDER BY id;

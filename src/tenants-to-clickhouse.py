import os
import requests
from clickhouse_driver import Client
from dotenv import load_dotenv

load_dotenv()


# Carica le variabili d'ambiente
PB_API_URL = os.getenv("PB_API_URL")
PB_USER = os.getenv("PB_USER")
PB_PASSWORD = os.getenv("PB_PASSWORD")
CLICKHOUSE_HOST = os.getenv("CLICKHOUSE_HOST")
CLICKHOUSE_DB = os.getenv("CLICKHOUSE_DB")
CLICKHOUSE_TABLE = os.getenv("CLICKHOUSE_TABLE")


# Step 1: Ottenere il Bearer Token
def get_bearer_token():
    auth_url = f"{PB_API_URL}/collections/users/auth-with-password"
    auth_payload = {
        "identity": PB_USER,
        "password": PB_PASSWORD
    }
    response = requests.post(auth_url, json=auth_payload)
    response.raise_for_status()  # Solleva un errore in caso di risposta errata
    print(f'Token: {response.json().get("token")}')
    return response.json().get("token")


# Step 2: Ottenere i record paginati da /collections/sdc_tenants/records
def get_sdc_tenants_records(token):
    records_url = f"{PB_API_URL}/collections/sdc_tenants/records"
    headers = {
        "Authorization": f"Bearer {token}",
        "Accept": "application/json"
    }

    records = []
    page = 1
    while True:
        response = requests.get(records_url, headers=headers, params={"page": page})
        response.raise_for_status()
        data = response.json()
        items = data.get("items", [])

        if not items:
            break

        records.extend(items)
        print(f"Processed page {page}")
        page += 1

    return records


# Step 3: Filtrare e costruire URL per la chiamata finale
def build_tenant_info_urls(records):
    urls = []
    for record in records:
        if record.get("current_url") == "production":
            url = record.get("production_url")
        elif record.get("current_url") == "temporary":
            url = record.get("temporary_url")
        else:
            continue

        if url:
            tenant_info_url = f"{url}/api/tenants/info"
            print(f"Built url: {url}")
            urls.append(tenant_info_url)

    return urls


# Step 4: Ottenere id e name dai tenant info
def get_tenant_info(urls):
    headers = {
        "Accept": "application/json"
    }

    tenant_info = []
    for url in urls:
        try:
            response = requests.get(url, headers=headers)
            response.raise_for_status()
            data = response.json()
            tenant_info.append({
                "id": data.get("id"),
                "name": data.get("name")
            })
            print(f"Got tenant info: {data.get('id')} - {data.get('name')}")
        except requests.exceptions.RequestException as e:
            print(f"Errore nella richiesta a {url}: {e}")
            continue  # Passa all'URL successivo

    return tenant_info


# Step 5: Scrivere i dati su ClickHouse
def write_to_clickhouse(tenant_info):
    print("Inserimento dati su ClickHouse")
    client = Client(host=CLICKHOUSE_HOST)

    # Inserire i dati
    client.execute(f"INSERT INTO {CLICKHOUSE_DB}.{CLICKHOUSE_TABLE} (id, name) VALUES", tenant_info)


def main():
    try:
        # Step 1: Ottenere il Bearer Token
        token = get_bearer_token()

        # Step 2: Ottenere i record dai tenants
        records = get_sdc_tenants_records(token)

        # Step 3: Costruire le URL per le chiamate info
        urls = build_tenant_info_urls(records)

        # Step 4: Ottenere le informazioni dai tenants
        tenant_info = get_tenant_info(urls)

        # Step 5: Scrivere le informazioni su ClickHouse
        write_to_clickhouse(tenant_info)
        print("Dati scritti con successo su ClickHouse.")

    except Exception as e:
        print(f"Errore: {e}")


if __name__ == "__main__":
    main()
